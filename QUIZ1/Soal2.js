import React, {useState, createContext} from 'react';
import {View, Text, FlatList} from 'react-native';

export const RootContext = createContext();

const ContextAPI = () => {
  const [name, setName] = useState([
    {
      name: 'Zakky Muhammad Fajar',
      position: 'Trainer 1 React Native Lanjutan',
    },
    {
      name: 'Mukhlis Hanafi',
      position: 'Trainer 2 React Native Lanjutan',
    },
  ]);

  return (
    <RootContext.Provider value={name}>
      <FlatList
        data={name}
        keyExtractor={(item) => String(item.name)}
        renderItem={({item}) => (
          <View>
            <Text>{item.name}</Text>
            <Text>{item.position}</Text>
          </View>
        )}
      />
    </RootContext.Provider>
  );
};

export default ContextAPI;
